import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;

public class Example {

    private static WebDriver driver;

    private static void navigate(String webSite) {
        driver.get(webSite);
    }

    private static void checkPageVisitedByTitle(String title) {
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, title);
    }

    private static void checkPageDisplays(By element) {
        try {
            driver.findElement(element);

        } catch (NoSuchElementException e) {}
    }

    private static void search(By element, String searchString){
        WebElement searchInput = driver.findElement(element);
        searchInput.sendKeys(searchString);
        searchInput.submit();
    }


    public static void main(String[] args) throws IOException {

        System.out.println("Enter a string you want to search on page: ");
        Scanner sc = new Scanner(System.in);
        String searchString = sc.next();


        System.setProperty("webdriver.chrome.driver", "C:\\Users\\vpetrenco\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();


        navigate("https://www.google.com/");
        checkPageDisplays(By.xpath("//*[@name = 'q']"));
        search(By.xpath("//*[@name = 'q']"), searchString);

        //I will do a method for it tomorrow
        String findText = driver.findElement(By.tagName("body")).getText();
        int occupancy = StringUtils.countMatches(findText, searchString);
        System.out.println(occupancy);
        

//        int counter = driver.findElements(By.xpath("html/body//*[not(name() = 'script' or name() = 'span') and contains(text(), searchString)]")).size();
//        System.out.println("String " + searchString + " repeated " + counter + " times");

        driver.quit();

    }
}